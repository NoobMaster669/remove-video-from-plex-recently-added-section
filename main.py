import os 
import sys
import plexapi

from dotenv import load_dotenv
from plexapi.server import PlexServer


working_folder = os.path.dirname(os.path.realpath(__file__))
load_dotenv(f'{working_folder}/config.env')

baseurl = os.environ.get("PLEX_HOST")
token = os.environ.get("PLEX_TOKEN")
plex = PlexServer(baseurl, token)

library = plex.library.section(os.environ.get("PLEX_LIBRARY"))

titles = open('data.txt', 'r')
titles = titles.readlines()

for title_content in titles:
    split_contents = title_content.strip().split("||")
    try:
        video = library.get(title=split_contents[0])
        if len(split_contents) > 1:
            updates = {"addedAt.value": split_contents[1]}
        else:
            updates = {"addedAt.value": os.environ.get("PLEX_DEFAULT_ADDED_DATE")}
        video.edit(**updates)
    except:
        print(f"Failed to Identify Movie: {split_contents[0]}")
    