# Remove Video From Plex Recently Added Section

## Setup
- `pip install requirements.txt`
- Rename `config.env.sample` to `config.env` and fill the contents
- Rename `data.txt.sample` to `data.txt`
- Provide list of titles (movies, tv show, songs etc) in the data.txt file.
    - Each show can be given its own added date from the `data.txt` file
    - Template <SHOW_NAME>||<ADDED_DATE>
> Please note that the added date in `data.txt` file is optional. If not provided then the value of `PLEX_DEFAULT_ADDED_DATE` from `config.env` will be used
- Run with `python3 main.py`